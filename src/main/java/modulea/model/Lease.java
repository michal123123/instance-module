package modulea.model;

public class Lease {
    private String start;
    private String end;
    private int cpu;
    private int ram;
    private int disk;
    private String leaseName;
    private String vmName;

    public Lease(String start, String end, int cpu, int ram, int disk, String leaseName, String vmName) {
        this.start = start;
        this.end = end;
        this.cpu = cpu;
        this.ram = ram;
        this.disk = disk;
        this.leaseName = leaseName;
        this.vmName = vmName;
    }


    @Override
    public String toString() {
        return super.toString() + "modulA.Lease{" +
                "start='" + start + '\'' +
                ", end='" + end + '\'' +
                ", cpu=" + cpu +
                ", ram=" + ram +
                ", disk=" + disk +
                ", leaseName='" + leaseName + '\'' +
                ", vmName='" + vmName + '\'' +
                '}';
    }

    public String getVmName() {
        return vmName;
    }

    public String getLeaseName() {
        return leaseName;
    }

    public String getEnd() {
        return end;
    }

    public String getStart() {
        return start;
    }

    public int getCpu() {
        return cpu;
    }

    public int getRam() {
        return ram;
    }

    public int getDisk() {
        return disk;
    }
}
