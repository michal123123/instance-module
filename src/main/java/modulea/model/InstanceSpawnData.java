package modulea.model;

public class InstanceSpawnData {
    private final String time;
    private final String serverGroupId;
    private final String flavorId;
    private final String imageId;
    private final String networkId;
    private final String vmName;
    private final String keypair;


    public InstanceSpawnData(String time, String serverGroupId, String flavorId,
                             String imageId, String networkId, String vmName, String keypair) {
        this.time = time;
        this.serverGroupId = serverGroupId;
        this.flavorId = flavorId;
        this.imageId = imageId;
        this.networkId = networkId;
        this.vmName = vmName;
        this.keypair = keypair;
    }

    public String getTime() {
        return time;
    }

    public String getImageId() {
        return imageId;
    }

    public String getNetworkId() {
        return networkId;
    }

    public String getServerGroupId() {
        return serverGroupId;
    }

    public String getFlavorId() {
        return flavorId;
    }

    public String getKeypair() {
        return keypair;
    }

    public String getVmName() {
        return vmName;
    }

    @Override
    public String toString() {
        return "InstanceSpawnData{" +
                "time='" + time + '\'' +
                ", serverGroupId='" + serverGroupId + '\'' +
                ", flavorId='" + flavorId + '\'' +
                ", imageId='" + imageId + '\'' +
                ", networkId='" + networkId + '\'' +
                ", vmName='" + vmName + '\'' +
                ", keypair='" + keypair + '\'' +
                '}';
    }
}