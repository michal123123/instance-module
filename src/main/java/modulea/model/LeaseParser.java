package modulea.model;

import org.apache.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class LeaseParser {
    private static Logger LOGGER = Logger.getLogger(LeaseParser.class);

    //start,end,cpu,ram,disk,leaseName,vmName
    public static List<Lease> parseLeasesFromFile(File file) throws IOException {
        if (file == null || !file.exists()) {
            String msg = "No lease file found!";
            LOGGER.error(msg);
            throw new IllegalArgumentException(msg);
        }

        List<Lease> result = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] attributes = line.split(",");
                if (attributes.length != 7) {
                    String errMsg = "Incorrect syntax of lease file";
                    Logger.getLogger(errMsg);
                    throw new IllegalArgumentException(errMsg);
                }
                String startDate = attributes[0];
                String endDate = attributes[1];
                int cpu = Integer.valueOf(attributes[2]);
                int ram = Integer.valueOf(attributes[3]);
                int disk = Integer.valueOf(attributes[4]);
                String leaseName = attributes[5];
                String vmName = attributes[6];
                result.add(new Lease(startDate, endDate, cpu, ram, disk, leaseName, vmName));
            }
        }
        return result;
    }
}
