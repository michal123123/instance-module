package modulea;

import com.jcraft.jsch.*;
import modulea.model.InstanceSpawnData;
import modulea.model.Lease;
import modulea.model.LeaseParser;
import modulea.tasks.LogInCreateInstanceTask;
import modulea.tasks.RunModuleBTask;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class InstanceModule {
    public static ScheduledExecutorService scheduler =
            Executors.newScheduledThreadPool(1);
    public  static String WORKSPACE = System.getenv("OPENSTACK_WORKSPACE");
    private static Logger LOGGER = Logger.getLogger(InstanceModule.class);
    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-M-dd hh:mm");
    private static File f = new File(WORKSPACE + "/planFor" + LocalDate.now().plusDays(1));
    private static File ids = new File(WORKSPACE + "/ids");
    private static final String SERVER_GROUP_ID = "server_group_id";
    private static byte[] tmp = new byte[5000];
    private static InputStream in;
    private static OutputStream out;
    private static final String FLAVOR_ID = "flavor_id";
    private static String networkId;
    private static String imageId;
    private static PropertiesFromFile properties;
    private static String keyPair;
    private static String host;
    private static int port = 22;
    private static String user;
    private static String passwd;
    private static Set<LocalDateTime> runningModuleBTimes = new TreeSet<>();
    private static LocalDateTime exitTime;

    //start:23:50
    //end: next day 23:15
    public static void main(String[] arg) {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        exitTime = LocalDateTime.now().plusDays(1).withHour(23).withMinute(15);
        LOGGER.info("Module A starting...");
        LOGGER.info("Exit time is " + exitTime);
        properties = PropertiesFromFile.getInstance(ids);
        user = properties.getProperty(PropertiesFromFile.USERNAME);
        passwd = properties.getProperty(PropertiesFromFile.PASSWD);
        host =  properties.getProperty(PropertiesFromFile.HOST);

        try {
            JSch jsch = new JSch();
            LOGGER.info("Connecting to " + user + "@" + host + ":" + port);
            Session session = jsch.getSession(user, host, port);
            session.setPassword(passwd);
            UserInfo ui = new SshUserInfo();
            session.setUserInfo(ui);
            session.setConfig("StrictHostKeyChecking", "no");
            session.connect(30000);   // making a connection with timeout.
            Channel channel = session.openChannel("exec");
            ((ChannelExec) channel).setCommand("sudo -S -p '' su");
            in = channel.getInputStream();
            out = channel.getOutputStream();
            ((ChannelExec) channel).setErrStream(System.err);
            channel.connect();
            readOutput();
            writeCommand(passwd);
            readOutput();
            writeCommand("su stack");
            readOutput();
            writeCommand("whoami");
            readOutput();
            writeCommand("cd /opt/stack/devstack");
            readOutput();
            writeCommand("pwd");
            readOutput();
            writeCommand(". openrc admin demo");
            Thread.sleep(5 * 1000);
            readOutput();
            LOGGER.info("Connected succsfully to" + host);
            scheduleRunningModuleBFullHours();
            LOGGER.info("Module B scheduled for(full hours)" + runningModuleBTimes);
            List<InstanceSpawnData> taskList = readAndCreateLeases();
            try {
                scheduleSpawnTask(taskList);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            LOGGER.info("Tasks scheduled successfully. Module A starts second phase.");
            tmp = new byte[1024];

            while (true) {
                while (in.available() > 0) {
                    int i = in.read(tmp, 0, 1024);
                    if (i < 0) break;
                    LOGGER.info(new String(tmp, 0, i));
                }
                try {
                    LOGGER.info("Sleeping for 5 minutes...");
                    Thread.sleep(5 * 60 * 1000);
                    if (LocalDateTime.now().isAfter(exitTime)) {
                        scheduler.shutdown();
                        break;
                    }

                } catch (Exception ee) {
                    LOGGER.error(ee.getMessage());
                }
            }

            channel.disconnect();
            session.disconnect();
            LOGGER.info("Module A exiting.");
        } catch (Exception e) {
            LOGGER.error(e.getMessage() + e.getStackTrace());
        }
    }

    private static void scheduleRunningModuleBFullHours() {
        LocalDate tomorrow = LocalDate.now().plusDays(1);
        for (int i = 1; i < 24; i++) {
            LocalDateTime hour = LocalDateTime.of(tomorrow, LocalTime.of(i, 0, 0));
            RunModuleBTask runModuleBTask = new RunModuleBTask();
            scheduler.schedule(runModuleBTask, hour.toInstant(ZoneOffset.ofTotalSeconds(0)).toEpochMilli()
                    - System.currentTimeMillis(), TimeUnit.MILLISECONDS);
            runningModuleBTimes.add(hour);
        }
    }

    private static List<InstanceSpawnData> readAndCreateLeases() throws IOException, InterruptedException {
        LOGGER.info("Parsing leases from file+" + f.getAbsolutePath() + "...");
        List<Lease> l = LeaseParser.parseLeasesFromFile(f);
        LOGGER.info("Successfully parsed " + l.size() + " leases.");
        for (Lease lease : l) {
            LOGGER.info(lease);
        }
        return reserveLeases(l);
    }

    public static String readOutput() throws IOException {
        while (in.available() > 0) {
            int i = in.read(tmp, 0, 1024);
            if (i < 0) break;

            String output = new String(tmp, 0, i);
            System.out.print(output);
            return output;
        }
        return null;
    }

    private static void writeCommand(String command) throws IOException {
        System.out.println(command);
        out.write((command + "\n").getBytes());
        out.flush();
    }

    private static List<InstanceSpawnData> reserveLeases(List<Lease> leases) throws IOException, InterruptedException {
        imageId = properties.getProperty(PropertiesFromFile.IMAGE_ID);
        networkId = properties.getProperty(PropertiesFromFile.NETWORK_ID);
        keyPair = properties.getProperty(PropertiesFromFile.KEYPAIR);
        /*
        blazar lease-create --reservation
        resource_type=virtual:instance,
        vcpus=1,
        memory_mb=1024,
        disk_gb=20,
        amount=1,
        affinity=False
        --start-date "2017-09-24 20:00"
        --end-date "2020-08-09 21:00"
        lease-1
         */
        LOGGER.info("Creating leases in Blazar...");
        List<InstanceSpawnData> result = new ArrayList<>();
        for (Lease l : leases) {
            String comm = "blazar lease-create --reservation" +
                    " resource_type=virtual:instance," +
                    "vcpus=" + l.getCpu() + "," +
                    "memory_mb=" + l.getRam() + "," +
                    "disk_gb=" + l.getDisk() + "," +
                    "amount=1," +
                    "affinity=False" +
                    " --start-date \"" + l.getStart() + "\"" +
                    " --end-date \"" + l.getEnd() + "\" " +
                    l.getLeaseName();
            LOGGER.info(comm);
            writeCommand(comm);
            Thread.sleep(5 * 1000);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 11; i++) {
                sb.append(readOutput());
            }
            String output = sb.toString();
            String sgid = attrId(output, SERVER_GROUP_ID);
            String flavorid = attrId(output, FLAVOR_ID);
            InstanceSpawnData task = new InstanceSpawnData(l.getStart(), sgid, flavorid, imageId, networkId, l.getVmName(), keyPair);
            result.add(task);
        }
        return result;
    }

    private static void scheduleSpawnTask(List<InstanceSpawnData> instanceSpawnDataList) throws ParseException {
        LOGGER.info("Scheduling spawn tasks...");
        for (InstanceSpawnData spawnData : instanceSpawnDataList) {
            Date date = sdf.parse(spawnData.getTime());
            LogInCreateInstanceTask task = new LogInCreateInstanceTask(spawnData);

            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            LOGGER.info(task);
            long oneMinute = 60_000L;
            Date scheduledTime = new Date(date.getTime() + oneMinute);
            LOGGER.info("Instance spawn request scheduled for " + dateFormat.format(scheduledTime));
            scheduler.schedule(task, scheduledTime.getTime()
                    - System.currentTimeMillis(), TimeUnit.MILLISECONDS);
            //SCHEDULE RUNNING MODULE B
            long minutes = Long.valueOf(PropertiesFromFile.getProperty(PropertiesFromFile.MODULE_B_DELAY));
            LOGGER.info("Module B delay is set to " + minutes + " minutes.");
            long moduleBDelayMilis = minutes * 60L * 1000L;
            Date moduleBRunning = new Date(date.getTime() + moduleBDelayMilis);
            LOGGER.info("Running module B scheduled for:" + dateFormat.format(moduleBRunning));
            LocalDateTime moduleBRunningDateTime =
                    LocalDateTime.ofInstant(Instant.ofEpochMilli(moduleBRunning.getTime()), ZoneId.of("UTC"));
            if (!runningModuleBTimes.contains(moduleBRunningDateTime)) {
                RunModuleBTask runModuleBTask = new RunModuleBTask();
                scheduler.schedule(runModuleBTask, moduleBRunning.getTime() - System.currentTimeMillis(),
                        TimeUnit.MILLISECONDS);
                LOGGER.info("Added to scheduler.");
                runningModuleBTimes.add(moduleBRunningDateTime);
            } else {
                LOGGER.info("Module B is already scheduled to run on this date, skipping.");
            }
        }
    }

    private static String attrId(String output, String attr) {
        String help = "\"" + attr + "\": \"";
        int startIdx = output.indexOf(help) + help.length();
        int endIdx = output.indexOf("\"", startIdx + 1);
        return output.substring(startIdx, endIdx);
    }
}