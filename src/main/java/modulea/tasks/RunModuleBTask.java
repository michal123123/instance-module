package modulea.tasks;

import com.jcraft.jsch.*;
import modulea.SshUserInfo;
import modulea.PropertiesFromFile;
import modulea.InstanceModule;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class RunModuleBTask implements Runnable {
    private static Logger LOGGER =
            Logger.getLogger(RunModuleBTask.class);
    private String host;
    private String user;
    private String passwd;
    private final byte[] tmp = new byte[5000];
    private final String runJarCommand =
            "sudo java -DOPENSTACK_WORKSPACE=${OPENSTACK_WORKSPACE} " +
                    "-Duser.timezone=Europe/London -Xmx64m -jar " +
                    InstanceModule.WORKSPACE + "/msc-module-b.jar " + " &";
    private InputStream in;
    private OutputStream out;

    @Override
    public void run() {
        user = PropertiesFromFile.getProperty(PropertiesFromFile.USERNAME);
        passwd = PropertiesFromFile.getProperty(PropertiesFromFile.PASSWD);
        host = PropertiesFromFile.getProperty(PropertiesFromFile.HOST);
        LOGGER.info("Running module B task starting...");
        JSch jsch = new JSch();
        Session session = null;
        try {
            session = jsch.getSession(user, host, 22);
        } catch (JSchException e) {
            e.printStackTrace();
            return;
        }
        session.setPassword(passwd);
        UserInfo ui = new SshUserInfo();
        session.setUserInfo(ui);
        session.setConfig("StrictHostKeyChecking", "no");
        try {
            LOGGER.info("Connecting to " + user + "@" + host);
            session.connect(7000);
            Channel channel = session.openChannel("exec");
            ((ChannelExec) channel).setCommand("sudo -S -p '' su");
            in = channel.getInputStream();
            out = channel.getOutputStream();
            ((ChannelExec) channel).setErrStream(System.err);
            channel.connect();
            writeCommand(passwd);
            readOutput();
            writeCommand("su stack");
            readOutput();
            writeCommand("whoami");
            readOutput();
            LOGGER.info("Running command " + runJarCommand);
            writeCommand(runJarCommand);
            writeCommand("disown");
            Thread.sleep(5_000);
            readOutput();
            LOGGER.info("Disconnecting...");
        } catch (Exception e) {
            LOGGER.error(e.getMessage() + e.getStackTrace());
            e.printStackTrace();
        }
    }

    private String readOutput() throws IOException {
        while (in.available() > 0) {
            int i = in.read(tmp, 0, 1024);
            if (i < 0) break;

            String output = new String(tmp, 0, i);
            System.out.print(output);
            return output;
        }
        return null;
    }


    private void writeCommand(String command) throws IOException {
        out.write((command + "\n").getBytes());
        out.flush();
    }
}
