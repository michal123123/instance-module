package modulea.tasks;

import com.jcraft.jsch.*;
import modulea.SshUserInfo;
import modulea.PropertiesFromFile;
import modulea.InstanceModule;
import modulea.model.InstanceSpawnData;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.apache.log4j.Logger;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import static org.apache.http.protocol.HTTP.USER_AGENT;

public class LogInCreateInstanceTask implements Runnable {
    private static Logger LOGGER = Logger.getLogger(LogInCreateInstanceTask.class);
    private final InstanceSpawnData task;
    private InputStream in;
    private OutputStream out;
    private static Object lockForFreeIpsRequest;
    private static File shortInitFile = new File(InstanceModule.WORKSPACE + "/shortInitFile");
    private String host;
    private String user;
    private String passwd;
    private final byte[] tmp = new byte[5000];

    static {
        lockForFreeIpsRequest = new Object();
    }

    public LogInCreateInstanceTask(InstanceSpawnData task) {
        user = PropertiesFromFile.getProperty(PropertiesFromFile.USERNAME);
        passwd = PropertiesFromFile.getProperty(PropertiesFromFile.PASSWD);
        host = PropertiesFromFile.getProperty(PropertiesFromFile.HOST);
        this.task = task;
    }

    @Override
    public void run() {
        JSch jsch = new JSch();
        Session session = null;
        try {
            session = jsch.getSession(user, host, 22);
        } catch (JSchException e) {
            e.printStackTrace();
            return;
        }
        session.setPassword(passwd);
        UserInfo ui = new SshUserInfo();
        session.setUserInfo(ui);
        session.setConfig("StrictHostKeyChecking", "no");
        try {
            LOGGER.info("Connecting to " + user + "@" + host);
            session.connect(7000);
            Channel channel = session.openChannel("exec");
            ((ChannelExec) channel).setCommand("sudo -S -p '' su");

            in = channel.getInputStream();
            out = channel.getOutputStream();
            ((ChannelExec) channel).setErrStream(System.err);
            channel.connect();

            writeCommand(passwd);
            readOutput();

            writeCommand("su stack");
            readOutput();

            writeCommand("whoami");
            readOutput();

            writeCommand("cd /opt/stack/devstack");
            readOutput();

            writeCommand("pwd");
            readOutput();

            writeCommand(". openrc admin demo");
            readOutput(60);

            String cloudInit = " --user-data " + shortInitFile.getAbsolutePath();
            String com = "openstack server create --flavor " + task.getFlavorId() + " --image " + task.getImageId() +//" --network " + task.getNetworkId() +
                    " --hint group=" + task.getServerGroupId() +
                    " --key-name " + task.getKeypair() + cloudInit + " " + task.getVmName();
            writeCommand(com);
            String output = readOutput(60);
            while (output.length() < 4400) {
                output += readOutput(60);
            }
            String serverId = getServerId(output).trim();
            LOGGER.info("Parsed serverId:>>>" + serverId + "<<<");
            ///token===============================================
            writeCommand("openstack token issue");
            Thread.sleep(10_000);

            String tokenOuput = readOutput(60);
            while (tokenOuput.length() < 1600) {
                tokenOuput += readOutput(60);
            }
            String token = getAuthTokenFromOuput(tokenOuput);
            LOGGER.info("Parsed token " + token);
            ///////////////////////////////////////////////////////////
            String serverPortId = getServerPortId(token, serverId);
            LOGGER.info("ServerPort id: " + serverPortId);
            synchronized (lockForFreeIpsRequest) {
                AddrAndIp res = getFreeIpId(token);
                String freeIpId = res.id;
                LOGGER.info("Parsed ip id:" + freeIpId);
                writeCommand("openstack floating ip set --port " + serverPortId + " " + freeIpId);
                LOGGER.info("Scheduling install libs task...");
                scheduleInstallLibsTask(res.ip);
            }
            channel.disconnect();
            session.disconnect();
        } catch (Exception e) {
            LOGGER.error(e.getMessage() + e.getStackTrace());
            e.printStackTrace();
        }
        LOGGER.info("Task executed successfully.");
    }

    private void scheduleInstallLibsTask(String freeIpId) throws java.text.ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-M-dd hh:mm");
        Date date = sdf.parse(task.getTime());
        long installDateMilis = date.getTime() +
                Long.valueOf(PropertiesFromFile.getProperty(
                        PropertiesFromFile.LIBS_INSTALL_DELAY)) * 1000L * 60L;
        LOGGER.info("Install libs task scheduled for:" + new Date(installDateMilis).toString());
        InstanceModule.scheduler.schedule(new InstallLibsTask(freeIpId),
                installDateMilis - System.currentTimeMillis(), TimeUnit.MILLISECONDS);
    }

    private String readOutput() throws IOException {
        while (in.available() > 0) {
            int i = in.read(tmp, 0, 1024);
            if (i < 0) break;

            String output = new String(tmp, 0, i);
            System.out.print(output);
            return output;
        }
        return null;
    }

    private AddrAndIp getFreeIpId(String token) throws IOException {
        String ending = "v2.0/floatingips";
        String beginning = PropertiesFromFile.getProperty(PropertiesFromFile.NETWORK_ENDPOINT);
        if (beginning.lastIndexOf('/') != beginning.length() - 1) {
            throw new IllegalArgumentException();
        }
        String url = beginning + ending;
        HttpURLConnection con = (HttpURLConnection)
                new URL(url).openConnection();
        con.setRequestMethod("GET");
        //add request header
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("X-Auth-Token", token);
        int responseCode = con.getResponseCode();
        LOGGER.info("Sending 'GET' request to URL : " + url);
        LOGGER.info("Response Code : " + responseCode);
        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        String out = response.toString();
        LOGGER.info(out);
        return getIdOfFreeip(out);
    }

    private String getServerId(String output) {
        int lineId = output.indexOf("| id ");
        int idIdx = lineId + 40;
        int idEndIdx = idIdx + 36;
        return output.substring(idIdx, idEndIdx);
    }

    private String readCharacters() throws IOException {
        while (in.available() > 0) {
            int i = in.read(tmp, 0, 1024);
            if (i < 0) break;
            String output = new String(tmp, 0, i);
            return output;
        }
        return null;
    }

    @Override
    public String toString() {
        return "LogInCreateInstanceTask{" +
                "task=" + task + '}';
    }

    static String getAuthTokenFromOuput(String output) {//183 characters
        int tokenIdx = output.indexOf("| id         | ") + 15;
        return output.substring(tokenIdx, tokenIdx + 183);
    }

    class AddrAndIp {
        String ip;
        String id;
    }

    private AddrAndIp getIdOfFreeip(String restOuput) {
        JSONParser parser = new JSONParser();
        try {
            JSONObject obj = (JSONObject) parser.parse(restOuput);
            JSONArray array = (JSONArray) obj.get("floatingips");
            for (Object floatingIp : array) {
                JSONObject obj2 = (JSONObject) floatingIp;
                String port = (String) obj2.get("port_id");
                if (port == null) {
                    AddrAndIp result = new AddrAndIp();
                    result.id = (String) obj2.get("id");
                    result.ip = (String) obj2.get("floating_ip_address");
                    return result;
                }
            }
        } catch (ParseException pe) {
            LOGGER.error(pe.getMessage() + pe.getStackTrace());
        }
        throw new NullPointerException();
    }


    private static String getServerPortId(String token, String serverId) throws IOException, ParseException {
        LOGGER.info("Executing request for /compute/v2.1/servers/{server-id}/os-interface");
        String beginning = PropertiesFromFile.getProperty(PropertiesFromFile.COMPUTE_ENDPOINT);
        if (beginning.lastIndexOf('/') != beginning.length() - 1) {
            throw new IllegalArgumentException();
        }
        //property computeEndpoint,http://192.168.1.41/compute/v2.1/ 
        //http://192.168.1.41/compute/v2.1/servers/{server-id}/os-interface
        String ending = "servers/" + serverId + "/os-interface";
        String url = beginning + ending;
        LOGGER.info("Generated URL for GET " + url);
        HttpURLConnection con = (HttpURLConnection)
                new URL(url).openConnection();
        con.setRequestMethod("GET");
        //add request header
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("X-Auth-Token", token);
        int responseCode = con.getResponseCode();
        LOGGER.info("Sending 'GET' request to URL : " + url);
        LOGGER.info("Response Code : " + responseCode);
        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        //if openstack responds late:
        //https://codereview.stackexchange.com/questions/45819/httpurlconnection-response-code-handling
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        String out = response.toString();
        LOGGER.info(out);
        return getPortFromJson(out);
    }

    private static String getPortFromJson(String out) throws ParseException {
        JSONParser parser = new JSONParser();
        JSONObject json = (JSONObject) parser.parse(out);
        JSONArray arr = (JSONArray) json.get("interfaceAttachments");
        JSONObject firstElem = (JSONObject) (arr.get(0));
        return (String) firstElem.get("port_id");
    }

    private String readOutput(int timeoutSecs) throws Exception {
        String s = null;
        do {
            s = readCharacters();
            if (s == null || s.length() == 0) {
                if (timeoutSecs == 0) {
                    throw new Exception("Timeout exceeded!");
                }
                Thread.sleep(8000);
                timeoutSecs--;
            } else {
                break;
            }

        } while (true);
        LOGGER.info("Output:" + s);
        return s;
    }

    private void writeCommand(String command) throws IOException {
        LOGGER.info("Command:" + command);
        out.write((command + "\n").getBytes());
        out.flush();
    }
}
