package modulea.tasks;

import com.jcraft.jsch.*;
import modulea.InstanceModule;
import org.apache.log4j.Logger;

import java.io.*;

public class InstallLibsTask implements Runnable {
    private static Logger LOGGER = Logger.getLogger(InstallLibsTask.class);
    private static final String SFTP_LOCAL_DIR = InstanceModule.WORKSPACE + File.separator + "libs";
    private static final String LIB_FILE1 = "libaio1_0.3.110-2_amd64.deb";
    private static final String LIB_FILE2 = "stress-ng_0.05.23-1_amd64.deb";
    private static final int SFTPPORT = 22;
    private static final String RSA_KEY_PATH = InstanceModule.WORKSPACE + File.separator + "keyPair";
    private static final String SPAWNED_VM_USER = "ubuntu";

    private final String ip;

    public InstallLibsTask(String ip) {
        this.ip = ip;
    }

    @Override
    public void run() {
        Session session = null;
        LOGGER.info("Starting...");
        try {
            JSch jsch = new JSch();
            jsch.addIdentity(RSA_KEY_PATH);
            LOGGER.info("Connecting to " + SPAWNED_VM_USER + "@" + ip + ":" + SFTPPORT);
            session = jsch.getSession(SPAWNED_VM_USER, ip, SFTPPORT);
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);
            session.connect();
            LOGGER.info("Connected.");
            sendLibs(session);
            installLibs(session);
            LOGGER.info("Task finished successfully.");
        } catch (Exception e) {
            LOGGER.error(e);
        } finally {
            session.disconnect();
            LOGGER.info("Session disconnected.");
        }
    }


    private void sendLibs(Session session) throws FileNotFoundException, SftpException, JSchException, InterruptedException {
        Channel channel = null;
        ChannelSftp channelSftp = null;
        try {
            channel = session.openChannel("sftp");
            channel.connect();
            LOGGER.info("SFTP channel opened and connected.");
            channelSftp = (ChannelSftp) channel;
            File f = new File(SFTP_LOCAL_DIR + File.separator + LIB_FILE1);
            LOGGER.info("Sending file:" + f.getAbsolutePath());
            channelSftp.put(new FileInputStream(f), f.getName());
            Thread.sleep(10_000);
            File f2 = new File(SFTP_LOCAL_DIR + File.separator + LIB_FILE2);
            LOGGER.info("Sending file:" + f2.getAbsolutePath());
            channelSftp.put(new FileInputStream(f2), f2.getName());
            Thread.sleep(15_000);
            LOGGER.info("Sending finished successfully.");
        } finally {
            channelSftp.exit();
            LOGGER.info("Sftp Channel exited.");
        }
    }


    private void installLibs(Session session) throws JSchException, IOException, InterruptedException {
        byte[] tmp = new byte[5000];
        LOGGER.info("Installing libs...");
        executeCommand(session, "sudo dpkg -i libaio1_0.3.110-2_amd64.deb", tmp);
        executeCommand(session, "sudo dpkg -i stress-ng_0.05.23-1_amd64.deb", tmp);
        LOGGER.info("Install finished successfully.");
    }

    private void executeCommand(Session session, String command, byte[] tmp) throws JSchException, IOException, InterruptedException {
        ChannelExec channelExec;
        Channel channel = session.openChannel("exec");
        channelExec = (ChannelExec) channel;
        channelExec.setErrStream(System.err);

        InputStream in = channel.getInputStream();
        OutputStream out = channel.getOutputStream();
        channelExec.setCommand(command);
        channel.connect();
        Thread.sleep(60_000);
        readOutput(in, tmp);
    }

    private String readOutput(InputStream in, byte[] tmp) throws IOException {
        while (in.available() > 0) {
            int i = in.read(tmp, 0, 1024);
            if (i < 0) break;

            String output = new String(tmp, 0, i);
            System.out.print(output);
            return output;
        }
        return null;
    }
}
