package modulea;

import org.apache.log4j.Logger;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class PropertiesFromFile {
    private File propFile;
    private static Map<String, String> propertiesMap;
    private static PropertiesFromFile singleton;
    private static org.apache.log4j.Logger LOGGER = Logger.getLogger(PropertiesFromFile.class);

    public static String NETWORK_ID = "networkId";
    public static String IMAGE_ID = "imageId";
    public static String KEYPAIR = "keyPair";
    public static String NETWORK_ENDPOINT = "networkEndpoint";
    public static String USERNAME = "username";
    public static String PASSWD = "passwd";
    public static String COMPUTE_ENDPOINT = "computeEndpoint";
    public static String MODULE_B_DELAY =  "moduleBDelay";
    public static String LIBS_INSTALL_DELAY = "libsInstalationDelay";
    public static String HOST = "host";

    private PropertiesFromFile(File propFile) {
        LOGGER.info("Reading properties from "+propFile.getAbsolutePath());
        if (propFile == null || !propFile.exists()) {
            LOGGER.error("Missing properties file!");
            throw new IllegalArgumentException();
        }
        propertiesMap = new HashMap<>();
        this.propFile = propFile;
        readFile();
        LOGGER.info("Expecting 10 properties");
        if (propertiesMap.size() != 10) {
            String msg = "Not enough properties";
            LOGGER.info(msg);
            throw new IllegalStateException(msg);
        }
        LOGGER.info("Properties loaded succesfully.");

    }

    public static synchronized PropertiesFromFile getInstance(File propFile) {
        if (singleton != null) {
            return singleton;
        }
        singleton = new PropertiesFromFile(propFile);
        return singleton;
    }

    public static String getProperty(String propName) {
        if (propertiesMap == null) {
            throw new IllegalStateException();
        }
        return propertiesMap.get(propName);
    }

    private void readFile() {
        try (BufferedReader br = new BufferedReader(new FileReader(propFile))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] attributes = line.split(",");
                if (attributes.length != 2) {//single line should have 2 parts
                    throw new IllegalArgumentException("Incorrect propertiesMap file");
                }
                String name = attributes[0];
                String value = attributes[1];
                if (value != null) {
                    propertiesMap.put(name, value);
                }
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage()+e.getStackTrace());
            e.printStackTrace();
        }
    }
}
